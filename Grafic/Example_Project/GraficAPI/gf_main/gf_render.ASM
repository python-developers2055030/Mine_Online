proc gf_RenderBegin, CameraPos, CameraTurn 
  ;�������� ������� ������
  invoke  glMatrixMode, GL_MODELVIEW
  invoke  glLoadIdentity
  stdcall gf_get_camera_lookvec, [CameraPos], [CameraTurn] 
  stdcall Matrix.LookAt, [CameraPos], gf_camera_lookvec, gf_uplookvec
  invoke glGetFloatv, GL_MODELVIEW_MATRIX, gf_view_matrix              
  
  ;�������� ������� ������ 
  invoke glUniformMatrix4fv, [gf_view_M], 1, 0, gf_view_matrix

  ret
endp


proc  gf_renderObj3D uses esi, Obj3D_Handle, tx_Handle,\
                               PosV, TurnV, Scale   

  ;�������� ������� (ModelMatrix)
  stdcall gf_CreateModelMatrix, [PosV], [TurnV], [Scale]
  invoke glUniformMatrix4fv, [gf_model_M], 1, 0, gf_model_matrix

  ;�������� ��������
  ;invoke glBindTexture, GL_TEXTURE_2D, [tx_Handle]
  ;invoke glUniform1i, [Tex1], 0
  
  ;�������� �������
  mov esi, [Obj3D_Handle]
  invoke glBindVertexArray, [esi]
  ;������
  invoke glDrawArrays, GL_TRIANGLES, 0, [esi + 4]
  
  ret
endp
                          

proc gf_RenderEnd 
  invoke SwapBuffers, [hdc]
  invoke glClear, GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT 
  
  ret
endp